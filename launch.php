<?php
require('Cards.php');
require('Player.php');

$cards = new Cards(52);
$playerOne = new Player();
$playerTwo = new Player();

//I don't want the same name!!
while ($playerTwo->getName() == $playerOne->getName()) {
    $playerTwo->setName();
}

echo 'Welcome in the battle between ' . $playerOne->getName() . " and " . $playerTwo->getName() . "\n";

echo 'Card shuffling' . "\n";
$cards->shuffleCards();

sleep(1);

echo "Distribution of cards" . "\n";

// on vas distribuer les carte une a une pour que ce soit encore mieux mélangé !

$cardsMixed = $cards->getCard();
$deckPlayerOne = [];
$deckPlayerTwo = [];

foreach ($cardsMixed as $key => $value) {
    if ($key % 2 != 0) {
        $deckPlayerOne[] = $value;
    } else {
        $deckPlayerTwo[] = $value;
    }
}
$playerOne->setDeck($deckPlayerOne);
$playerTwo->setDeck($deckPlayerTwo);

sleep(1);

echo "let's play ! " . "\n\n";

$turn = 0;


// tans que les deux joueurs ont des carte on joue

while ($playerOne->getDeck() != NULL and $playerTwo->getDeck() != NULL) {


// on prend la premiere carte des 2 deck, on les compare, on ajoute un point au Winner, et on les suprime du deck
    roundOfGame($playerOne, $playerTwo, $turn);

    $turn++;
//    on temporise pour les resources
    sleep(1);
}

//END OF GAMMMMEE!
echo ($playerOne->getPV() > $playerTwo->getPV()) ? $playerOne->getName() . " is the big winner" : '';
echo ($playerOne->getPV() < $playerTwo->getPV()) ? $playerTwo->getName() . " is the big winner" : '';
echo ($playerOne->getPV() == $playerTwo->getPV()) ? "C'est une égaliter parfaite" : '';

echo "\n";


function roundOfGame($playerOne, $playerTwo, $turn)
{

    echo $playerOne->getName() . " joue " . $playerOne->getDeck()[$turn] . " et " . $playerTwo->getName() . " joue le " . $playerTwo->getDeck()[$turn] . "\n";
    if ($playerOne->getDeck()[$turn] > $playerTwo->getDeck()[$turn]) {
        echo $playerOne->getName() . " Win" . "\n";
        $playerOne->addPV();
    } elseif ($playerOne->getDeck()[$turn] < $playerTwo->getDeck()[$turn]) {
        echo $playerTwo->getName() . " Win" . "\n";
        $playerTwo->addPV();
    } else {
        echo "Okay, Houston, I believe we've had a problem here" . "\n";
//        https://en.wikipedia.org/wiki/Houston,_we_have_a_problem
    }

    $playerOne->deleteFirstCard($turn);
    $playerTwo->deleteFirstCard($turn);

    echo "\n";
}
