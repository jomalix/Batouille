# Test technique, recréer un jeux de bataille simplifier


## Énoncer:

il s'agit de coder en PHP objet une simulation de jeu de bataille (le jeu de carte) dont les règles ont été simplifiées :

* 52 cartes, on simplifie en utilisant simplement des valeurs de 1 à 52
* les cartes sont mélangées et distribuées à 2 joueurs
* chaque joueur retourne la première carte de son paquet, le joueur disposant de la plus forte carte marque un point
* on continue jusqu'à ce qu'il n'y ait plus de carte à jouer
* on affiche le nom du vainqueur

C'est donc à coder en PHP objet sans framework ni library et à lancer en ligne de commande.

Le but est :
* de voir comment tu design le tout de manière claire et facilement maintenable et évolutive.
* de nous donner un aperçu de ce que tu peux faire techniquement

## Installation:

Besoin juste de PHP 7 au minimum.
Lancer le fichier launch.php

    php launch.php

Et le tour est jouer!

En attendant impatiemment votre retour!