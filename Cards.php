<?php

class Cards
{
    public array $card = [];

    public function __construct(int $nbCards)
    {
        $i = 1;
        $array = [];
        while ($i <= $nbCards) {
            $array[] = $i;
            $i++;
        }
        $this->setCard($array);
    }

// Shuffle the cards
    public function shuffleCards()
    {
        $array = $this->getCard();
        shuffle($array);
        $this->setCard($array);
    }

    //    Getter & setter

    /**
     * @return mixed
     */
    public function getCard()
    {
        return $this->card;
    }

    /**
     * @return mixed
     */
    public function setCard(array $value)
    {
        $this->card = $value;
    }


}