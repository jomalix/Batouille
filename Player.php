<?php


class Player
{
    public int $PV;
    public string $name;
    public array $deck = [];


    public function __construct()
    {
        $this->setName();
        $this->setPV(0);
    }


//    win one PV
    public function addPV()
    {
        $PV = $this->getPV();
        $PV++;
        $this->setPV($PV);
        echo $this->getName() . " a maintenant " . $PV . " PV." . "\n";
    }

// Carte jouer, carte plus en jeux!
    public function deleteFirstCard($turn)
    {
        $array = $this->getDeck();
        unset($array[$turn]);
        $this->setDeck($array);
    }

//    Getter & setter


    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(): void
    {
        $this->name = $this->getRrandomName();
    }


    /**
     * @return int
     */
    public function getPV(): int
    {
        return $this->PV;
    }

    /**
     * @param int $PV
     */
    public function setPV(int $PV): void
    {
        $this->PV = $PV;
    }

    /**
     * @return array
     */
    public function getDeck(): array
    {
        return $this->deck;
    }

    /**
     * @param array $deck
     */
    public function setDeck(array $deck): void
    {
        $this->deck = $deck;
    }

    private function getRrandomName()
    {
        $randomName = ["Noé", "Alban", "Julie", "Benjamin", "Mathis", "Faustine", "Olivier", "Julien", "Léna", "Tim", "Laure"];
        return $randomName[array_rand($randomName)];
    }
}